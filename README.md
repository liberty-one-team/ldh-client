# Liberty CLI

[project] | [code] | [tracker] | [pypi]

*Liberty CLI* is a user-facing command-line client for interacting
with Librem One or another Liberty Deckplan Host (LDH).

Dive into https://liberty.one for LDH design and development.

## Installation

The preferred way to install Liberty CLI is with your package
manager. The recommended package name is `ldh-client`. For example:

```bash
sudo apt install ldh-client # Debian-based
```

or

```bash
pipx install ldh-client # Python-based
```

## Usage

For help and a list of available commands:

```bash
liberty --help
```

To add a tunnel connection to Network-Manager:

```bash
liberty setup tunnel
# you will be prompted for your Librem One/LDH credentials
```

## Installation (from source)

If you'd prefer to run from source...

1. Install Python 3.x and pipenv. (See
   <https://docs.pipenv.org/install/> for a tutorial.)

2. Install prerequisites:

        apt install libcairo2-dev libgirepository1.0-dev libssl-dev python3-dev

3. Get source:

        git clone https://source.puri.sm/liberty/tool/client.git ldh_client

4. Install with pipenv:

        cd ldh_client
        pipenv install --dev -e .

## Usage (from source)

```bash
cd ldh_client
pipenv run liberty --help
# using "pipenv run" avoids confusing the package and source installations
```

## Build wheel package (and optionally upload)

Follow these instructions to build Liberty CLI as a Python package:

```bash
git clone https://source.puri.sm/liberty/tool/client.git ldh_client
cd ldh_client
pipenv install --dev
pipenv shell
# optionally edit default.strict.yaml
python setup.py sdist bdist_wheel
```

If everything works as expected you should end up with the files:

* `dist/ldh_client-<version>-py3-none-any.whl`
* `dist/ldh_client-<version>.tar.gz`

You can now optionally upload the created Python package to PyPI using twine:

```bash
twine upload dist/*
```

## Sharing and contributions

Liberty CLI (LDH client)  
<https://source.puri.sm/liberty/tool/client>  
Copyright 2018-2020 Purism SPC  
SPDX-License-Identifier: AGPL-3.0-or-later  

Shared under AGPL-3.0-or-later. We adhere to the Community Covenant
1.0 without modification, and certify origin per DCO 1.1 with a
signed-off-by line. Contributions under the same terms are welcome.

For details see:

* [COPYING.AGPL.md], full license text
* [CODE_OF_CONDUCT.md], full conduct text
* [CONTRIBUTING.DCO.md], full origin text (`git -s`)

<!-- Links -->

[project]: https://source.puri.sm/liberty/tool/client
[code]: https://source.puri.sm/liberty/tool/client/tree/master
[tracker]: https://source.puri.sm/liberty/tool/client/issues
[pypi]: https://pypi.org/project/ldh-client/
[SETUP.md]: SETUP.md
[COPYING.AGPL.md]: COPYING.AGPL.md
[CODE_OF_CONDUCT.md]: CODE_OF_CONDUCT.md
[CONTRIBUTING.DCO.md]: CONTRIBUTING.DCO.md
[COPYING.md]: COPYING.md
[CONTRIBUTING.md]: CONTRIBUTING.md
