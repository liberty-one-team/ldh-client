# Copying notice

Liberty CLI
<https://source.puri.sm/liberty/tool/client>
Copyright 2017-2020 Purism SPC
SPDX-License-Identifier: AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Notices for included code

All code is covered by and compatible with the "Copying notice" above.
Some code copied from other sources comes with additional notices. These
are listed below, and also appear in the files where that code appears.

---

Network-Manager tunnel setup code includes some code from
Network-Manager Python examples:  
https://gitlab.freedesktop.org/NetworkManager/NetworkManager/blob/master/examples/python/gi/vpn-import.py  
Copyright 2014 Red Hat, Inc.  
SPDX-License-Identifier: GPL-2.0-or-later

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Copyright 2014 Red Hat, Inc.
