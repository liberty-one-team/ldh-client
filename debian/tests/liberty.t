use strict;
use warnings;

use Test::More;
use Test::Command::Simple;

my $CMD = 'liberty';

run_ok $CMD;
like stdout, qr/^Usage: $CMD \[OPTIONS\] COMMAND \[ARGS\]\.\.\./, 'bare command, stdout';
cmp_ok stderr, 'eq', '', 'bare command, stderr';

run_ok $CMD, '--help';
like stdout, qr/^Usage: $CMD \[OPTIONS\] COMMAND \[ARGS\]\.\.\./, 'help, stdout';
cmp_ok stderr, 'eq', '', 'help, stderr';

run_ok 2, $CMD, qw(foobar);
cmp_ok stdout, 'eq', '', 'foobar, stdout';
like stderr, qr/Error: No such command "foobar"/, 'foobar, stderr';

run_ok $CMD, qw(setup);
like stdout, qr/^Usage: $CMD setup \[OPTIONS\] COMMAND \[ARGS\]\.\.\./, 'setup, stdout';
cmp_ok stderr, 'eq', '', 'setup, stderr';

run_ok 2, $CMD, qw(setup foobar);
cmp_ok stdout, 'eq', '', 'setup foobar, stdout';
like stderr, qr/Error: No such command "foobar"/, 'setup foobar, stderr';

done_testing;
